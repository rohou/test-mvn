grammar Hello;

stog : expres+;

expres : PART VALUE SLOT (SIZE SIZEVALUE SLOT)?;

floatNumber: SIZEVALUE;

NUMBER : [0]|[1-9]+[0-9]*;
PLUS : 'p'|'plus';
SUB : 's' ;
MUL : 'm' ;
DIV : 'd' ;
PART : '（左侧甲状腺）' | '（右侧甲状腺）' ;
VALUE : '桥本性甲状腺炎伴结节性甲状腺肿' | '微小状乳头状癌伴钙化';
SIZE : '大小约';
SIZEVALUE : (FLOAT ('cm'|'*')?)+;
FLOAT: [0-9]+('.'[0-9]+)?;
SLOT : '。' |  '，' ;
WS : [ \t\n\r]+ -> skip ;