package com.jlh.antlr;


import com.jlh.antlr.source.HelloLexer;
import com.jlh.antlr.source.HelloMeListener;
import com.jlh.antlr.source.HelloParser;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import java.io.IOException;

/**
 * @author mymx.jlh
 * @version 1.0.0 2018/6/6 9:58
 */
public class TestMain {
    public static void main(String[] args) throws IOException {
        String hello = "（左侧甲状腺）桥本性甲状腺炎伴结节性甲状腺肿。（右侧甲状腺）微小状乳头状癌伴钙化，大小约0.5*0.3cm。";
        ANTLRInputStream inputStream = new ANTLRInputStream(hello);
        HelloLexer lexer = new HelloLexer(inputStream);
        CommonTokenStream tokenStream = new CommonTokenStream(lexer);

        HelloParser parser = new HelloParser(tokenStream);

        ParseTree parseTree = parser.stog();

        HelloMeListener helloMeListener = new HelloMeListener();

        ParseTreeWalker parseTreeWalker = new ParseTreeWalker();
        parseTreeWalker.walk(helloMeListener,parseTree);

        System.out.println(parseTree);
    }
}
