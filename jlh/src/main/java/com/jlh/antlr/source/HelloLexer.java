// Generated from C:/D-drive-78765/work/workSpace/test-mvn/jlh/src/main/java/com/jlh/antlr\Hello.g4 by ANTLR 4.7
package com.jlh.antlr.source;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class HelloLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NUMBER=1, PLUS=2, SUB=3, MUL=4, DIV=5, PART=6, VALUE=7, SIZE=8, SIZEVALUE=9, 
		FLOAT=10, SLOT=11, WS=12;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"NUMBER", "PLUS", "SUB", "MUL", "DIV", "PART", "VALUE", "SIZE", "SIZEVALUE", 
		"FLOAT", "SLOT", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'s'", "'m'", "'d'", null, null, "'\u6FB6\u0443\u76AC\u7EFE\uFFFD'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NUMBER", "PLUS", "SUB", "MUL", "DIV", "PART", "VALUE", "SIZE", 
		"SIZEVALUE", "FLOAT", "SLOT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public HelloLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Hello.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\16\u00a0\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\3\2\3\2\6\2\36\n\2\r\2\16\2\37\3\2\7\2#\n\2\f"+
		"\2\16\2&\13\2\5\2(\n\2\3\3\3\3\3\3\3\3\3\3\5\3/\n\3\3\4\3\4\3\5\3\5\3"+
		"\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7"+
		"\3\7\3\7\3\7\3\7\3\7\3\7\5\7M\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b"+
		"\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\bu\n\b\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\n\3\n\3\n\3\n\5\n\u0081\n\n\6\n\u0083\n\n\r\n\16\n\u0084"+
		"\3\13\6\13\u0088\n\13\r\13\16\13\u0089\3\13\3\13\6\13\u008e\n\13\r\13"+
		"\16\13\u008f\5\13\u0092\n\13\3\f\3\f\3\f\3\f\5\f\u0098\n\f\3\r\6\r\u009b"+
		"\n\r\r\r\16\r\u009c\3\r\3\r\2\2\16\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n"+
		"\23\13\25\f\27\r\31\16\3\2\6\3\2\62\62\3\2\63;\3\2\62;\5\2\13\f\17\17"+
		"\"\"\2\u00ad\2\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2"+
		"\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2"+
		"\27\3\2\2\2\2\31\3\2\2\2\3\'\3\2\2\2\5.\3\2\2\2\7\60\3\2\2\2\t\62\3\2"+
		"\2\2\13\64\3\2\2\2\rL\3\2\2\2\17t\3\2\2\2\21v\3\2\2\2\23\u0082\3\2\2\2"+
		"\25\u0087\3\2\2\2\27\u0097\3\2\2\2\31\u009a\3\2\2\2\33(\t\2\2\2\34\36"+
		"\t\3\2\2\35\34\3\2\2\2\36\37\3\2\2\2\37\35\3\2\2\2\37 \3\2\2\2 $\3\2\2"+
		"\2!#\t\4\2\2\"!\3\2\2\2#&\3\2\2\2$\"\3\2\2\2$%\3\2\2\2%(\3\2\2\2&$\3\2"+
		"\2\2\'\33\3\2\2\2\'\35\3\2\2\2(\4\3\2\2\2)/\7r\2\2*+\7r\2\2+,\7n\2\2,"+
		"-\7w\2\2-/\7u\2\2.)\3\2\2\2.*\3\2\2\2/\6\3\2\2\2\60\61\7u\2\2\61\b\3\2"+
		"\2\2\62\63\7o\2\2\63\n\3\2\2\2\64\65\7f\2\2\65\f\3\2\2\2\66\67\7\u951d"+
		"\2\2\678\7\u581f\2\289\7\u4e51\2\29:\7\u6e1c\2\2:;\7\u0447\2\2;<\7\u6575"+
		"\2\2<=\7\u941a\2\2=>\7\u60f2\2\2>?\7\u5407\2\2?@\7\u951d\2\2@M\7\uffff"+
		"\2\2AB\7\u951d\2\2BC\7\u581f\2\2CD\7\u5f7a\2\2DE\7\u6e1c\2\2EF\7\u0447"+
		"\2\2FG\7\u6575\2\2GH\7\u941a\2\2HI\7\u60f2\2\2IJ\7\u5407\2\2JK\7\u951d"+
		"\2\2KM\7\uffff\2\2L\66\3\2\2\2LA\3\2\2\2M\16\3\2\2\2NO\7\u5999\2\2OP\7"+
		"\u30e8\2\2PQ\7\u6e72\2\2QR\7\u93ae\2\2RS\7\u0447\2\2ST\7\u6575\2\2TU\7"+
		"\u941a\2\2UV\7\u60f2\2\2VW\7\u5407\2\2WX\7\u9412\2\2XY\7\u5e9f\2\2YZ\7"+
		"\u5375\2\2Z[\7\u7f03\2\2[\\\7\u64bb\2\2\\]\7\u59af\2\2]^\7\u93ae\2\2^"+
		"_\7\u0447\2\2_`\7\u6575\2\2`a\7\u941a\2\2ab\7\u60f2\2\2bc\7\u5407\2\2"+
		"cd\7\u9474\2\2du\7\uffff\2\2ef\7\u5bf2\2\2fg\7\ue1be\2\2gh\7\u76ae\2\2"+
		"hi\7\u941a\2\2ij\7\u6737\2\2jk\7\u94ab\2\2kl\7\u6fb8\2\2lm\7\u5bfa\2\2"+
		"mn\7\u59fa\2\2no\7\u9429\2\2op\7\u5c7e\2\2pq\7\u5375\2\2qr\7\u95bf\2\2"+
		"rs\7\u6b11\2\2su\7\u5bf4\2\2tN\3\2\2\2te\3\2\2\2u\20\3\2\2\2vw\7\u6fb8"+
		"\2\2wx\7\u0445\2\2xy\7\u76ae\2\2yz\7\u7f00\2\2z{\7\uffff\2\2{\22\3\2\2"+
		"\2|\u0080\5\25\13\2}~\7e\2\2~\u0081\7o\2\2\177\u0081\7,\2\2\u0080}\3\2"+
		"\2\2\u0080\177\3\2\2\2\u0080\u0081\3\2\2\2\u0081\u0083\3\2\2\2\u0082|"+
		"\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085"+
		"\24\3\2\2\2\u0086\u0088\t\4\2\2\u0087\u0086\3\2\2\2\u0088\u0089\3\2\2"+
		"\2\u0089\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u0091\3\2\2\2\u008b\u008d"+
		"\7\60\2\2\u008c\u008e\t\4\2\2\u008d\u008c\3\2\2\2\u008e\u008f\3\2\2\2"+
		"\u008f\u008d\3\2\2\2\u008f\u0090\3\2\2\2\u0090\u0092\3\2\2\2\u0091\u008b"+
		"\3\2\2\2\u0091\u0092\3\2\2\2\u0092\26\3\2\2\2\u0093\u0094\7\u9288\2\2"+
		"\u0094\u0098\7\uffff\2\2\u0095\u0096\7\u951d\2\2\u0096\u0098\7\uffff\2"+
		"\2\u0097\u0093\3\2\2\2\u0097\u0095\3\2\2\2\u0098\30\3\2\2\2\u0099\u009b"+
		"\t\5\2\2\u009a\u0099\3\2\2\2\u009b\u009c\3\2\2\2\u009c\u009a\3\2\2\2\u009c"+
		"\u009d\3\2\2\2\u009d\u009e\3\2\2\2\u009e\u009f\b\r\2\2\u009f\32\3\2\2"+
		"\2\20\2\37$\'.Lt\u0080\u0084\u0089\u008f\u0091\u0097\u009c\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}