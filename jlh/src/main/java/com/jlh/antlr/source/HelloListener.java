// Generated from C:/D-drive-78765/work/workSpace/test-mvn/jlh/src/main/java/com/jlh/antlr\Hello.g4 by ANTLR 4.7
package com.jlh.antlr.source;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HelloParser}.
 */
public interface HelloListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HelloParser#stog}.
	 * @param ctx the parse tree
	 */
	void enterStog(HelloParser.StogContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#stog}.
	 * @param ctx the parse tree
	 */
	void exitStog(HelloParser.StogContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#expres}.
	 * @param ctx the parse tree
	 */
	void enterExpres(HelloParser.ExpresContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#expres}.
	 * @param ctx the parse tree
	 */
	void exitExpres(HelloParser.ExpresContext ctx);
	/**
	 * Enter a parse tree produced by {@link HelloParser#floatNumber}.
	 * @param ctx the parse tree
	 */
	void enterFloatNumber(HelloParser.FloatNumberContext ctx);
	/**
	 * Exit a parse tree produced by {@link HelloParser#floatNumber}.
	 * @param ctx the parse tree
	 */
	void exitFloatNumber(HelloParser.FloatNumberContext ctx);
}