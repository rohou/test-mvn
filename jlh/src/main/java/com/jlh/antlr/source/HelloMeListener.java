package com.jlh.antlr.source;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * @author mymx.jlh
 * @version 1.0.0 2018/6/6 10:35
 */
public class HelloMeListener extends HelloBaseListener {
    private Stack<Integer> numStack = new Stack<>();
    private Queue<String> symbolQueue = new ConcurrentLinkedQueue<>();

    @Override
    public void enterExpres(HelloParser.ExpresContext ctx) {
        super.enterExpres(ctx);
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        super.enterEveryRule(ctx);
    }

    @Override
    public void visitTerminal(TerminalNode node) {
        super.visitTerminal(node);
        System.out.println(node.toString());
    }
}
