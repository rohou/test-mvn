// Generated from C:/D-drive-78765/work/workSpace/test-mvn/jlh/src/main/java/com/jlh/antlr\Hello.g4 by ANTLR 4.7
package com.jlh.antlr.source;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class HelloParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NUMBER=1, PLUS=2, SUB=3, MUL=4, DIV=5, PART=6, VALUE=7, SIZE=8, SIZEVALUE=9, 
		FLOAT=10, SLOT=11, WS=12;
	public static final int
		RULE_stog = 0, RULE_expres = 1, RULE_floatNumber = 2;
	public static final String[] ruleNames = {
		"stog", "expres", "floatNumber"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'s'", "'m'", "'d'", null, null, "'\u6FB6\u0443\u76AC\u7EFE\uFFFD'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NUMBER", "PLUS", "SUB", "MUL", "DIV", "PART", "VALUE", "SIZE", 
		"SIZEVALUE", "FLOAT", "SLOT", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Hello.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public HelloParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class StogContext extends ParserRuleContext {
		public List<ExpresContext> expres() {
			return getRuleContexts(ExpresContext.class);
		}
		public ExpresContext expres(int i) {
			return getRuleContext(ExpresContext.class,i);
		}
		public StogContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HelloListener ) ((HelloListener)listener).enterStog(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HelloListener ) ((HelloListener)listener).exitStog(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HelloVisitor ) return ((HelloVisitor<? extends T>)visitor).visitStog(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StogContext stog() throws RecognitionException {
		StogContext _localctx = new StogContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_stog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(7); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(6);
				expres();
				}
				}
				setState(9); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==PART );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExpresContext extends ParserRuleContext {
		public TerminalNode PART() { return getToken(HelloParser.PART, 0); }
		public TerminalNode VALUE() { return getToken(HelloParser.VALUE, 0); }
		public List<TerminalNode> SLOT() { return getTokens(HelloParser.SLOT); }
		public TerminalNode SLOT(int i) {
			return getToken(HelloParser.SLOT, i);
		}
		public TerminalNode SIZE() { return getToken(HelloParser.SIZE, 0); }
		public TerminalNode SIZEVALUE() { return getToken(HelloParser.SIZEVALUE, 0); }
		public ExpresContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expres; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HelloListener ) ((HelloListener)listener).enterExpres(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HelloListener ) ((HelloListener)listener).exitExpres(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HelloVisitor ) return ((HelloVisitor<? extends T>)visitor).visitExpres(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExpresContext expres() throws RecognitionException {
		ExpresContext _localctx = new ExpresContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_expres);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(11);
			match(PART);
			setState(12);
			match(VALUE);
			setState(13);
			match(SLOT);
			setState(17);
			_errHandler.sync(this);
			_la = _input.LA(1);
			if (_la==SIZE) {
				{
				setState(14);
				match(SIZE);
				setState(15);
				match(SIZEVALUE);
				setState(16);
				match(SLOT);
				}
			}

			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FloatNumberContext extends ParserRuleContext {
		public TerminalNode SIZEVALUE() { return getToken(HelloParser.SIZEVALUE, 0); }
		public FloatNumberContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_floatNumber; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof HelloListener ) ((HelloListener)listener).enterFloatNumber(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof HelloListener ) ((HelloListener)listener).exitFloatNumber(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof HelloVisitor ) return ((HelloVisitor<? extends T>)visitor).visitFloatNumber(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FloatNumberContext floatNumber() throws RecognitionException {
		FloatNumberContext _localctx = new FloatNumberContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_floatNumber);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(19);
			match(SIZEVALUE);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\16\30\4\2\t\2\4\3"+
		"\t\3\4\4\t\4\3\2\6\2\n\n\2\r\2\16\2\13\3\3\3\3\3\3\3\3\3\3\3\3\5\3\24"+
		"\n\3\3\4\3\4\3\4\2\2\5\2\4\6\2\2\2\26\2\t\3\2\2\2\4\r\3\2\2\2\6\25\3\2"+
		"\2\2\b\n\5\4\3\2\t\b\3\2\2\2\n\13\3\2\2\2\13\t\3\2\2\2\13\f\3\2\2\2\f"+
		"\3\3\2\2\2\r\16\7\b\2\2\16\17\7\t\2\2\17\23\7\r\2\2\20\21\7\n\2\2\21\22"+
		"\7\13\2\2\22\24\7\r\2\2\23\20\3\2\2\2\23\24\3\2\2\2\24\5\3\2\2\2\25\26"+
		"\7\13\2\2\26\7\3\2\2\2\4\13\23";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}