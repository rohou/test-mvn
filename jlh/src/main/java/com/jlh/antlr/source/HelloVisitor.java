// Generated from C:/D-drive-78765/work/workSpace/test-mvn/jlh/src/main/java/com/jlh/antlr\Hello.g4 by ANTLR 4.7
package com.jlh.antlr.source;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link HelloParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface HelloVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link HelloParser#stog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStog(HelloParser.StogContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#expres}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpres(HelloParser.ExpresContext ctx);
	/**
	 * Visit a parse tree produced by {@link HelloParser#floatNumber}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatNumber(HelloParser.FloatNumberContext ctx);
}