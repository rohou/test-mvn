package com.jlh.netty;

import java.io.Serializable;

/**
 * Created by jlh
 * On 2017/4/12 0012.
 */
public class Command implements Serializable{
    private String code;
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Command{" +
                "code='" + code + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
