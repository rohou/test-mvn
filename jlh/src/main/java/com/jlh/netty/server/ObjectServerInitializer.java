package com.jlh.netty.server;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * Created by jlh
 * On 2017/4/12 0012.
 */
public class ObjectServerInitializer extends ChannelInitializer<SocketChannel>{
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        socketChannel.pipeline().addLast(new ObjectEncoder()
                ,new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)
                ),new ObjectServerHandler());
    }
}
