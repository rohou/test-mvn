package com.jlh.viewer.NIO;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * com.jlh.viewer.NIO
 * Created by ASUS on 2017/4/22.
 * 13:33
 */
public class Main {
    public static void main(String[] args) throws IOException {

        FileInputStream fileInputStream= new FileInputStream("D:\\work\\workSpace\\test-mvn\\jlh\\src\\main\\java\\com\\jlh\\viewer\\gsonFomartTest.java");
        FileOutputStream fileOutputStream = new FileOutputStream("D:\\work\\workSpace\\test-mvn\\jlh\\src\\main\\java\\com\\jlh\\viewer\\out.txt");
        FileChannel fileChannel=fileInputStream.getChannel(),fileChannel1=fileOutputStream.getChannel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.clear();
        while (fileChannel.read(byteBuffer)!=-1){
            byteBuffer.flip();
            fileChannel1.write(byteBuffer);
            
            byteBuffer.clear();
        }

        fileChannel.close();
        fileChannel1.close();
        fileOutputStream.close();
        fileInputStream.close();
    }
}
