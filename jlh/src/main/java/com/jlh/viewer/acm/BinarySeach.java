package com.jlh.viewer.acm;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 16:10
 */
public class BinarySeach {
    public int getPos(int[] A, int n, int val) {
        // write code here
        int pos =search(A,0,n-1,val );
        int index =pos ;
        for (;index>=0;index--){
            if (A[pos]!=A[index])
               return index+1;
        }
        return 0;
    }
    public int search (int[] A,int left,int right,int val){
        int mid = (left+right)/2;
        if (A[mid]==val)
            return mid;
        else if (A[mid]<val)
            return search(A,mid+1,right,val);
        else
            return search(A,left,mid,val);
    }
    public static void main(String[] args) {
        BinarySeach binarySeach = new BinarySeach();
        System.out.println(binarySeach.getPos(new int[]{3,3,3,3,3},5,3));
    }
}
