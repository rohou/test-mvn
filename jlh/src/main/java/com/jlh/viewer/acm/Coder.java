package com.jlh.viewer.acm;

import java.util.*;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 16:31
 */
public class Coder {
    public String[] findCoder(String[] A, int n) {
        LinkedHashMap<Integer,Integer> mp = new LinkedHashMap<>();
        for (int i=0;i<n;i++){
            int num=getCodeNum(A[i]);
            if (num>0)
                mp.put(i,num);
        }
        ArrayList<Map.Entry<Integer,Integer>> list = new ArrayList<>(mp.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue())*-1;
            }
        });
        String[] res=new String [list.size()];
        for (int i=0;i<list.size();i++){
            res[i]=A[list.get(i).getKey()];
        }
        return res;
    }

    public int getCodeNum(String a){
        if (a.length()<5)
            return 0;
        int num=0;
        char [] as = a.toCharArray();
        for (int i=4;i<as.length;i++){
            if (as[i-4]=='c'||as[i-4]=='C'){
                if ((as[i-3]=='o'||as[i-3]=='O')&&(as[i-2]=='d'||as[i-2]=='D')&&(as[i-1]=='e'||as[i-1]=='E')&&(as[i]=='r'||as[i]=='R'))
                    num++;
            }
        }
        return num;
    }

    public static void main(String[] args) {
        Coder coder =new Coder();
        Arrays.stream(coder.findCoder(new String[]{"i am a coder","Coder Coder","Code"},3)).forEach(System.out::println);
    }
}
