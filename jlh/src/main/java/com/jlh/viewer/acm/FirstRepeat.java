package com.jlh.viewer.acm;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 16:22
 */
public class FirstRepeat {
    static boolean[] flag = new boolean[305];
    public void init (){
        for (int i=0;i<flag.length;i++)
            flag[i]=false;
    }
    public char findFirstRepeat(String A, int n) {
        init();
        char[] as = A.toCharArray();
        for (int i=0;i<n;i++){
            int index =as[i];
            if (flag[index])
                return as[i];
            else
                flag[index]=true;
        }
        return 0;
    }
}
