package com.jlh.viewer.acm;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 17:00
 */
public class Flip {
    int[][] dir= new int[][]{
            {-1, 0}, {1, 0}, {0, 1}, {0, -1}
    };

    public int[][] flipChess(int[][] A, int[][] f) {
        // write code here
        int n=A.length,m=A[0].length;
        for (int i=0;i<f.length;i++){
            int x=f[i][0]-1,y=f[i][1]-1;
            for (int j=0;j<dir.length;j++){
                int a = x+dir[j][0],b=y+dir[j][1];
                if ((a>=0&&a<n)&&(b>=0&&b<n)){
                    A[a][b]^=1;
                }
            }
        }
        return A;
    }

    public static void main(String[] args) {
        Flip flip =new Flip();
        flip.flipChess(new int[][]{{0,0,1,1},{1,0,1,0},{0,1,1,0},{0,0,1,0}},new int[][]{{2,2},{3,3},{4,4}});
    }
}
