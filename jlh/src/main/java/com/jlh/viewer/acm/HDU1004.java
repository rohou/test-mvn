package com.jlh.viewer.acm;

import java.util.Scanner;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/3/14.
 * 19:43
 */
public class HDU1004 {
    private static String index[]=new String[1005];
    private static int length;
    private static int count[]=new int[1005];
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (true){
            int n= sc.nextInt();
            if (n==0)
                break;
            sc.nextLine();
            init(n);
            for (int i=0;i<n;i++){
                String color = sc.nextLine();
                int j= isIndex(color);
                if (j==-1){
                    index[length]=color;
                    j=length++;
                }
                count[j]++;
            }
            int max=-1,maxIndex=-1;
            for (int i=0;i<length;i++){
                if (max<count[i]){
                    max=count[i];
                    maxIndex=i;
                }
            }
            System.out.println(index[maxIndex]);
        }
    }

    public static void  init(int n){
        for (int i=0;i<=n;i++){
            index[i]="";
            count[i]=0;
        }
        length=0;
    }

    public static int isIndex(String color){
        for (int i=0;i<length;i++){
            if (color.equals(index[i]))
                return i;
        }
        return -1;
    }
}
