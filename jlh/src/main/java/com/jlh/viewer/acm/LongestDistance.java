package com.jlh.viewer.acm;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 16:53
 */
public class LongestDistance {
    public int getDis(int[] A, int n) {
        // write code here
        int max =0;
        for (int i=0;i<n;i++){
            for (int j=i+1;j<n;j++){
                max=Math.max(A[j]-A[i],max);
            }
        }
        return max;
    }
}
