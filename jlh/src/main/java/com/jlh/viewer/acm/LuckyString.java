package com.jlh.viewer.acm;

import java.util.HashSet;
import java.util.Scanner;
import java.util.TreeSet;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/19.
 * 13:12
 */
public class LuckyString {
    static  boolean[] fib  = new boolean[105];

    public static int diff(String input){
        HashSet<String> tol =new HashSet<>();
        for (int i=0;i<input.length();i++)
            tol.add(String.valueOf(input.charAt(i)));
        return tol.size();
    }

    public static void main(String[] args) {
        fib[1]=true;
        fib[2]=true;
        int pre1=1,pre2=2;
        while (true){
            int sum= pre1+pre2;
            if (sum>=105)
                break;
            fib[sum]=true;
            pre1=pre2;
            pre2=sum;
        }
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()){
            String input =sc.nextLine();
            TreeSet<String> flag =new TreeSet<>();
            for (int i=0;i<input.length();i++){
                for (int j=i+1;j<=input.length();j++){
                    String sub=input.substring(i,j);
                    int diff = diff(sub);
                    if (fib[diff]&&!flag.contains(sub)){
                        flag.add(sub);
                    }
                }
            }
            for (String aa:flag){
                System.out.println(aa);
            }
        }
    }
}
