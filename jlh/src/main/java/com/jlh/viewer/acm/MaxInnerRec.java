package com.jlh.viewer.acm;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/21.
 * 15:12
 */
public class MaxInnerRec {
    public int countArea(int[] A, int n) {
        int max =-1;
        for (int i=0;i<n;i++){
            int num=1;
            int j=i-1;
            while (j>=0){
                if (A[j]>=A[i]) {
                    num++;
                    j--;
                }else
                    break;
            }
            j=i+1;
            while (j<n){
                if (A[j]>=A[i]) {
                    num++;
                    j++;
                }else
                    break;
            }
            max = Math.max(num*A[i],max);
        }
        return max;
    }

    public static void main(String[] args) {
        MaxInnerRec maxInnerRec = new MaxInnerRec();
        System.out.println(maxInnerRec.countArea(new int[]{281,179,386,165,88,500},6));
    }
}
