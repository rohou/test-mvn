package com.jlh.viewer.acm;

import java.util.Scanner;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/19.
 * 13:44
 */
public class NumbericKeyPad {
    static  int[][] flag ={
        {1,0,0,0,0,0,0,0,0,0},
        {1,1,1,1,1,1,1,1,1,1},
        {1,0,1,1,0,1,1,0,1,1},
        {0,0,0,1,0,0,1,0,0,1},
        {1,0,0,0,1,1,1,1,1,1},
        {1,0,0,0,0,1,1,0,1,1},
        {0,0,0,0,0,0,1,0,0,1},
        {1,0,0,0,0,0,0,1,1,1},
        {1,0,0,0,0,0,0,0,1,1},
        {0,0,0,0,0,0,0,0,0,1}
        };
    public static void main(String[] args) {
        Scanner sc  =new Scanner(System.in);
        int n  = sc.nextInt();
        sc.nextLine();
        for (;n>0;n--){
            char[] inp =sc.nextLine().toCharArray();
            while (true) {
                int i = 1;
                for (; i < inp.length ; i++) {
                    int p=inp[i-1]-'0';
                    int r=inp[i]-'0';
                    if (flag[p][r]!=1) {
                        break;
                    }
                }
                if (i<inp.length){
                    substrc(inp,i);
                }else {
                    System.out.println(new String (inp));
                    break;
                }
            }
        }

    }
    public static void substrc(char[] a,int k){
        int jw;//借位
        jw = -1;
        for(int i=k; i>=0&&jw!=0; i--){
        if(a[i]-'0'>0){
            a[i] += jw;
            jw = 0;
        }
        else{
            a[i] = '9';
            jw = -1;
        }
        for(int j=i+1;j<a.length;j++)//只要修改了还没到末尾数字，都需要将后面的置为9
            a[j] = '9';
        }
    }

}
