package com.jlh.viewer.acm;

import java.util.Scanner;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 15:33
 */
public class PokerCompare {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String input = sc.nextLine();
            String []pokers =  input.split("-");
            System.out.println(judge(pokers[0],pokers[1]));
        }
    }

    public static String judge(String a1,String a2){
        String b1[] = a1.split(" ");
        String b2[] = a2.split(" ");
        if (b1[0].toLowerCase().equals("joker")&&b1[1].toLowerCase().equals("joker"))
            return a1;
        if (b2[0].toLowerCase().equals("joker")&&b2[1].toLowerCase().equals("joker"))
            return a2;
        if (b1.length==4&&b2.length==4){
            if (b1[0].compareTo(b2[0])<0)
                return a2;
            else return a1;
        }
        if (b1.length==4)
            return a1;
        if (b2.length==4)
            return a2;
        if (b1.length==b2.length){
            if (getNum(b1[0])<getNum(b2[0]))
                return a2;
            else return a1;
        }
        return "ERROR";
    }

    public static int getNum(String a){
        switch (a){
            case "3":return 0;
            case "4":return 1;
            case "5":return 2;
            case "6":return 3;
            case "7":return 4;
            case "8":return 5;
            case "9":return 6;
            case "10":return 7;
            case "J":return 8;
            case "Q":return 9;
            case "K":return 10;
            case "A":return 11;
            default :return 12;
        }

    }
}
