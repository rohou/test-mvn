package com.jlh.viewer.acm;

import java.util.*;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 14:15
 */
public class SimpleErrorRecord {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        LinkedHashMap<String,Integer> mp = new LinkedHashMap<>();
        while (sc.hasNext()){
            String input = sc.next();
            if (input.equals("-1"))
                break;
            Integer line= sc.nextInt();
            String name = input.substring(input.lastIndexOf("\\")+1);
            if (mp.containsKey(name+" "+line)){
                mp.put(name+" "+line,mp.get(name+" "+line)+1);
            }else {
                mp.put(name+" "+line,1);
            }
        }
        ArrayList<Map.Entry<String,Integer>> a = new ArrayList<>(mp.entrySet());
        Collections.sort(a, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                int res= o1.getValue().compareTo(o2.getValue());
                return res*-1;
            }
        });
        int m=0;
        for (Map.Entry<String,Integer> entry:a){
            m++;
            if (m>8)
                break;
            String temp[]=entry.getKey().split(" ");
            System.out.println(temp[0].substring(Math.max(temp[0].length()-16,0),temp[0].length())+" "+temp[1]+" "+entry.getValue());

        }
    }
}
