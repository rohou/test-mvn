package com.jlh.viewer.acm;

import java.util.Scanner;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/19.
 * 17:40
 */
public class SpringOuting {
    private static int []score = new int[1005];
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()){
            int n=sc.nextInt(),k=sc.nextInt();
            int maxs=-1,mindex=-1;
            init(k);
            for (int i=0;i<n;i++){
                for (int j=k;j>=0;j--){
                    int inp= sc.nextInt();
                    score[inp]+=j;
                    if (maxs<score[inp]){
                        maxs=score[inp];
                        mindex=inp;
                    }else if (maxs==score[inp]){
                        mindex=0;
                    }
                }
            }

            System.out.println(mindex);

        }
    }
    public static void init(int s){
        for (int i=0;i<=s;i++){
            score[i]=0;
        }
    }
}
