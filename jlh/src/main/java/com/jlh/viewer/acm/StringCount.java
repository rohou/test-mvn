package com.jlh.viewer.acm;

import java.util.Scanner;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/21.
 * 15:30
 */
public class StringCount {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String a1 = sc.next(), a2 = sc.next();
            int l1 = sc.nextInt(), l2 = sc.nextInt();
            char[] b1 = a1.toCharArray(), b2 = a2.toCharArray();
            int dp[] =new int[120];
            int result=0;
            for(int i=1; i<=l2; i++){
                dp[i] = (26*(dp[i-1]))%1000007;
                if(i<=b1.length) dp[i] -=b1[i-1];
                if(i<=b2.length) dp[i] +=b2[i-1];
                if(i == b2.length) dp[i]--;
                if(i>=l1) result += dp[i];
            }
            System.out.println(result%1000007);
        }
    }
}
