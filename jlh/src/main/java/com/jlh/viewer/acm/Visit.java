package com.jlh.viewer.acm;

/**
 * com.jlh.viewer.acm
 * Created by ASUS on 2017/4/20.
 * 17:23
 */
public class Visit {
    public int countPath(int[][] map, int n, int m) {
        int a1=-1,b1=-1,a2=-1,b2=-1;
        int[][] dp=new int[n][m];
        for (int i=0;i<n;i++){
            for (int j=0;j<m;j++){
                if (map[i][j]==1)
                {
                    a1=i;b1=j;
                }else if (map[i][j]==2){
                    a2=i;b2=j;
                }
            }
        }
        if (a1==a2||b1==b2)
            return 1;
        int x=0,y=0;
        if (a1<a2)
            x=1;
        else
            x=-1;
        if (b1<b2)
            y=1;
        else
            y=-1;
        for (int i=0;i<n;i++){
            if (map[i][b1]!=-1)
                dp[i][b1]=1;
        }
        for (int i=0;i<m;i++){
            if (map[a1][i]!=-1)
                dp[a1][i]=1;
        }
        for (int i=a1+x;x==1?i<n:i>=0;i+=x){
            for (int j=b1+y;y==1?j<m:j>=0;j+=y){
                if (map[i][j]==-1)
                    dp[i][j]=0;
                else
                    dp[i][j]=dp[i-x][j]+dp[i][j-y];
            }
        }
        return dp[a2][b2];
    }

    public static void main(String[] args) {
        Visit visit = new Visit();
        System.out.println(visit.countPath(new int[][]{{0,1,0},{2,0,0}},2,3));
    }
}
