package com.jlh.viewer.algorithm;

import java.util.Arrays;

/**
 * com.jlh.viewer.algorithm
 * Created by ASUS on 2017/4/19.
 * 9:58
 */
public class BubbleSort {
    public static  void sort (int []a){
        boolean flag= true;
        int i=a.length-1;
        while (flag){
            flag=false;
            for (int j=0;j<i;j++){
                if (a[j]<a[j+1]){
                    a[j]=a[j]^a[j+1];
                    a[j+1]=a[j]^a[j+1];
                    a[j]=a[j]^a[j+1];
                    flag=true;
                }
            }
            i--;
        }
    }

    public static void main(String[] args) {
        int []a={3,1,2,5,7,2,8};
        BubbleSort.sort(a);
        Arrays.stream(a).forEach(System.out::println);
    }
}
