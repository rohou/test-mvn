package com.jlh.viewer.algorithm;

import java.util.Arrays;

/**
 * Created by jlh
 * On 2017/3/14 0014.
 */
public class MQueue {
    private Integer[] datas;
    private int front=0;
    private int back=0;
    private int store=0;
    public MQueue(int size) {
        this.datas = new Integer[size];
    }
    public boolean push (int d){
        if (!isFull()) {
            datas[back]=d;
            back=(back+1)%datas.length;
            store++;
            return true;
        }
        return false;
    }
    public Integer pop (){
        if (!isEmpty()){
            store--;
            Integer result=datas[front];
            front=(front+1)%datas.length;
            return result;
        }
        return null;
    }

    public boolean isEmpty(){
        return store == 0;
    }

    public boolean isFull(){
        return store == datas.length;
    }

    @Override
    public String toString() {
        return "MQueue{" +
                "datas=" + Arrays.toString(datas) +
                ", front=" + front +
                ", back=" + back +
                ", store=" + store +
                '}';
    }

    public static void main(String[] args) {
        MQueue mQueue = new MQueue(4);
        for (int i=0;i<100;i++){
            if (!mQueue.push(i)){
                mQueue.pop();
                mQueue.push(i);
            }
            System.out.println(mQueue);
        }

    }
}
