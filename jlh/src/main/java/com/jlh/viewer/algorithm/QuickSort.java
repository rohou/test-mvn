package com.jlh.viewer.algorithm;

import java.util.Random;

/**
 * Created by jlh
 * On 2017/3/14 0014.
 */
public class QuickSort {
    public void reslove (int []datas,int l,int r){
        if (datas.length==0||l>=r)
            return ;
        int key= datas[l];
        int st= l;
        int ed= r;
        while (st<ed){
            while (ed>st && datas[ed]>=key){
                ed--;
            }
            datas[st]=datas[ed];
            //此处是否有等于号无所谓，因为只要下个while循环至少能跳过那个交换后的值即可
            while (st<ed &&datas[st] <=key){
                st++;
            }
            datas[ed]= datas[st];
        }
        datas[st]=key;
        reslove(datas,l,st-1);
        reslove(datas,st+1,r);
    }

    public static void main(String[] args) {
        QuickSort quickSort = new QuickSort();
        int []datas = new int[10];
        Random random = new Random();
        for (int i=0;i<datas.length;i++){
            datas[i]=random.nextInt(100);
        }
        for (int i=0;i<datas.length;i++){
            System.out.print((i==0?"":",")+datas[i]);
        }
        quickSort.reslove(datas,0,datas.length-1);
        System.out.println();
        for (int i=0;i<datas.length;i++){
            System.out.print((i==0?"":",")+datas[i]);
        }
    }
}
