package com.jlh.viewer.base64;

import java.util.Base64;

/**
 * Created by jlh
 * On 2017/4/5 0005.
 */
class Base64Utils {
    static String encode(byte[] context){
        return Base64.getEncoder().encodeToString(context);
    }
    static byte[] decode(String context){
        return Base64.getDecoder().decode(context);
    }
}
