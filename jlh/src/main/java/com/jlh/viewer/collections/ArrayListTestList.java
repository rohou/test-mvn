package com.jlh.viewer.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.stream.Collectors;

/**
 * com.jlh.viewer
 * Created by ASUS on 2017/3/9.
 * 19:42
 */
public class ArrayListTestList {
    public static void main(String[] args) {
        //一般实现了RandomAccess接口的类用loop快，其余的用iterator快， RandomAccess是空接口，仅做标志，代表该类是通过数组实现，可以快速随机访问
        // 类似与Serializable接口也仅仅做标记，其中serialVersionUID代表类的版本号，版本号不符合时以便抛出异常，一旦修改版本号就代表对之前版本的类不再支持
        //list 与一般colleciton不同的地方多了个Listiterator 比iterator 多了可以修改的支持   iterator是快速失败的核心
        //capacity 默认10  默认1.5倍扩容，如果1.5倍还是要小直接扩容至最小的需要大小  vector 为2倍 且为线程安全
        int capacity=16;
        ArrayList<Integer> arrayList = new ArrayList<>(capacity);
        for (int i=0;i<17;i++){
            arrayList.add(i);
        }
        //压缩多余空余空间
        arrayList.trimToSize();
        System.out.println(arrayList.toArray().length);


        ArrayList<Integer> test = new ArrayList<>(Arrays.asList(1,1,1,2,3,4));

//        for (Integer a :test){ concurrentModificationException
//            if (a.equals(1))
//                test.remove(a);
//        }

//        Iterator<Integer> a = test.iterator();
//        ListIterator<Integer> b= test.listIterator();
//        Integer temp;
//        while (a.hasNext()){
//            if ((temp=a.next()).equals(1))
//                a.remove(); //必须通过迭代器删除
//        }
//        System.out.println(test);

        System.out.println(test.stream().filter(m->!m.equals(1)).collect(Collectors.toList()));
    }
}
