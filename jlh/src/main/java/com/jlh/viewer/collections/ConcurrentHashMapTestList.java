package com.jlh.viewer.collections;

import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by jlh
 * On 2017/3/15 0015.
 */
public class ConcurrentHashMapTestList {
    public static void main(String[] args) {

        /*
        *
        *   并发三大特性：
        *   原子性
        *   可见性，
        *   重排序
        *
        *   JDK1.8 以下
        *   concurrent 每次访问只对某一个segment加锁 使用lock 来实现，
        *   因此其他线程访问不同segment 不会阻塞，性能高
        *   segment --> tb1 -->tb2
        *   segment --> tb1
        *   segment -->
        *   JDK1.8
        *   concurrent Treebin对象，代替treenode ，包涵锁的功能，链表长了以后使用treebin对象，
        *   也就是代替了树的根节点，其余节点为treenode
        *   对段（segment）的锁定是通过 synchronized 来完成  （写操作）  读操作通过cas完成
        *   hashtable是对整个操作进行 synchronized 所以，每次操作都会锁整个对象，性能差
        * */
        Hashtable hashtable = new Hashtable();
        ConcurrentHashMap<String,String> concurrentHashMap = new ConcurrentHashMap<String,String>();
        for (int i=0;i<14;i++)
            concurrentHashMap.put("key_"+i,"huaizuo_" + i);
    }
}
