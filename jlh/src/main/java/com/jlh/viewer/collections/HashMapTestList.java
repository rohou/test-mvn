package com.jlh.viewer.collections;

import java.util.HashMap;

/**
 * com.jlh.test.viewer
 * Created by ASUS on 2017/3/8.
 * 19:40  jlh189
 */
public class HashMapTestList {
    public static void main(String[] args) {
        /*
         *   JDK 1.8
         *   concurrent 不再仅仅是数组+链表，而是数组+链表+红黑树 链表大于8自动转为红黑树
         */
        //linkedHashMap 与hashmap的区别是，增加了记录插入顺序，遍历时可以按插入顺序遍历
        // 负载0.75 默认16capacity      Entry bucket[]   ---> Entry-->Entry--->....Entry                 数组+链表   链表地址法
        // threshold（临界值） = 负载*capacity       iterator 保证快速失败-->  volatile modcount 保持可见性  新增和删除操作+1 修改不变
        HashMap<String,String> map=new HashMap<>();
        System.out.println( map.put("k1","1"));
        //先判断KEY是否为null 为null特殊处理放在bucket[0] 否则就二次hash 再取余    （先对象自己hash ，再hashmap 进行HASH）
        // hashmap 的hash 函数  h^=(h>>>20)^(h>>>12)
        //                      h=h^(h>>>7)^(h>>>4)
        String s;
        System.out.println((s=map.get("k1")).equals("1")?s:"b");
        // put操作，如果覆盖了会返回旧的值，如果没覆盖返回NULL
        System.out.println("oldValue:"+map.put("k1","2"));
        //  >>>符号位也参与右移操作，>>符号位不参与 没有<<<操作
        System.out.println(-8>>>2);
        // 因为hashmap中每次扩容 size都是2的倍数 ，所以 每次使用 index&(length-1) 的时候 就相当于是 index%length  ，虽然不理解为什么不直接用% 估计是出于性能考虑
        System.out.println(2&15);
        //每次扩容 需要新建对象 然后旧对象整体复制，性能低下


     }
}
