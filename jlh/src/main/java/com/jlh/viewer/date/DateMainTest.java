package com.jlh.viewer.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * com.jlh.viewer.date
 * Created by ASUS on 2017/5/9.
 * 14:37
 */
public class DateMainTest {
    private static final  String sepratorLine = "--------------------%s---------------------";

    public static void main(String[] args) {
        LocalDate localDate=LocalDate.now();
        System.out.println(localDate.atTime(LocalTime.of(15,30)));
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd",Locale.CHINESE);


        compareDate();
        compareLocalDateTime();
    }


    private static void compareDate(){
        try {
            System.out.println(String.format(sepratorLine,"compareDate"));
            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.CHINESE);
            Date secondDate = sdf.parse("06/30/2017");
            Date firstDate = sdf.parse("06/24/2017");
            long diffInMillies = Math.abs(firstDate.getTime()-secondDate.getTime());
            System.out.println(TimeUnit.DAYS.convert(diffInMillies,TimeUnit.MILLISECONDS));

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static void compareLocalDateTime(){
        System.out.println(String.format(sepratorLine,"compareLocalDateTime"));
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime pre = now.minusDays(6);

        Duration  duration = Duration.between(pre,now);

        System.out.println(Math.abs(duration.toDays()));
    }
}
