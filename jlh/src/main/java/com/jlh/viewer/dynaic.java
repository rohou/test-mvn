package com.jlh.viewer;

/**
 * com.jlh.viewer
 * Created by ASUS on 2017/4/19.
 * 16:39
 */
public class dynaic {
    public static void main(String[] args) {
        Son s = new Son();
        System.out.println(s.a);
        System.out.println(((Father)s).a);
        System.out.println(s.b);
        System.out.println(((Father)s).b);
        System.out.println(s.get());
        System.out.println(((Father)s).get());
        s.work();
        ((Father)s).work();
    }
    static class Father {
        int a=1;
        static int b=1;
        public int get(){
            return a;
        }
        public static void work(){
            System.out.println("father");
        }
    }

    static class Son extends Father{
        int a=2;
        static int b=2;
        public int get(){
            return a;
        }
        public static void work(){
            System.out.println("son");
        }
    }

}
