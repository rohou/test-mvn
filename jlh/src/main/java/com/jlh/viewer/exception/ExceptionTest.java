package com.jlh.viewer.exception;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by jlh
 * On 2017/4/7 0007.
 */
public class ExceptionTest {
    private static int x = 100;

    private static void add() {
    }

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        list.forEach(System.out::println);
        ExceptionTest exceptionTest = new ExceptionTest();
        try {
            throw new IOException("123");
        } catch (IOException e) {
            System.out.println("IOE");
        } catch (Exception e) {
            System.out.println("E");
        }
    }


    private class ass {
        void add() {
        }
    }
}
