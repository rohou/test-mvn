package com.jlh.viewer.genericity;

import java.util.ArrayList;
import java.util.List;

/**
 * com.jlh.viewer.genericity
 * Created by ASUS on 2017/4/28.
 * 8:58
 */
public class ExtendSuperTest {
    public static class A{

    }
    public static class  B extends  A{}
    public static class C extends B{}


    public static class D <T extends B>{
        T t;
    }


    public static void main(String[] args) {
        D d = new D<C>();
        D d1 = new D<B>();
//        D d2 = new D<A>();
        List<? super B> a =new ArrayList<>();
        a.add(new C());
    }
}
