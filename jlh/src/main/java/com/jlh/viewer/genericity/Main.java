package com.jlh.viewer.genericity;

/**
 * com.jlh.viewer.genericity
 * Created by ASUS on 2017/4/23.
 * 9:46
 */
public class Main {
    public static class Stack<T> {
        private int size;
        private int top=-1;
        private Object[] datas;

        public Stack(int size) {
            this.size=size;
            datas=new Object[size];
        }
        public T pop(){
            return (top >= 0) ? (T) datas[top--] : null;
        }
        public void push (T v){
            if (top+1<size)
                datas[++top]=v;
        }
    }
    public static void main(String[] args) {
        Stack<Integer> stack =new Stack<>(3);
        stack.push(2);
        System.out.println(stack.pop());
        stack.push(1);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        System.out.println(1);
    }
}
