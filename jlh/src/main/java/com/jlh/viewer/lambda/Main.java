package com.jlh.viewer.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Created by jlh
 * On 2017/4/11 0011.
 */
public class Main {
    public static void main(String[] args) {
        String []key ={"k1","k2","k3","k4","k5"};
        List<Integer> list= Arrays.asList(1,2,3,4,5);
        list.stream().collect(
                (Supplier<List<Integer>>) ArrayList::new,
                (integers, integer) -> integers.add(integer),
                List::addAll
        ).forEach(System.out::print);


        List<Person> people = new ArrayList<>();
        people.add(new Person("jlh",18));
        people.add(new Person("jlh",18));
        Map<String,List<Integer>> map= people.stream().collect(Collectors.groupingBy(Person::getName, Collectors.mapping(Person::getOld,Collectors.toList())));

    }
    public static class Person{
        private String name;
        private Integer old;

        public Person(String name, Integer old) {
            this.name = name;
            this.old = old;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getOld() {
            return old;
        }

        public void setOld(Integer old) {
            this.old = old;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    ", old=" + old +
                    '}';
        }
    }
}
