package com.jlh.viewer.memory;

/**
 * com.jlh.viewer.memory
 * Created by ASUS on 2017/3/14.
 * 22:07
 */
public class JavaMemoryLeak {
    //假定只有method 使用了data对象，其他方法都没使用
    //此处从某种意义上也算JAVA简单的内存泄漏
    //那么在method方法结束后，data的区域并不会被回收，只有在memory对象回收后才会被回收
    //总结 内存泄漏即 短生命周期对象被长生命对象持有，使段生命周期对象到了回收时间点却不被回收
    static class Memory{
        private Object data;
        public void method(){
            data=new Object();
        }
    }
}
