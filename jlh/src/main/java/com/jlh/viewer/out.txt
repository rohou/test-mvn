package com.jlh.viewer;

/**
 * Created by jlh
 * On 2017/4/11 0011.
 */
public class gsonFomartTest {


    /**
     * reason : 成功1
     * result : {"jobid":"2015120913503797592","realname":"商世界","bankcard":"6259656360701234","idcard":"310329198103050011","mobile":"18912341234","res":"2","message":"认证信息不匹配"}
     * error_code : 0
     */

    private String reason;
    private ResultBean result;
    private int error_code;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public ResultBean getResult() {
        return result;
    }

    public void setResult(ResultBean result) {
        this.result = result;
    }

    public int getError_code() {
        return error_code;
    }

    public void setError_code(int error_code) {
        this.error_code = error_code;
    }

    public static class ResultBean {
        /**
         * jobid : 2015120913503797592
         * realname : 商世界
         * bankcard : 6259656360701234
         * idcard : 310329198103050011
         * mobile : 18912341234
         * res : 2
         * message : 认证信息不匹配
         */

        private String jobid;
        private String realname;
        private String bankcard;
        private String idcard;
        private String mobile;
        private String res;
        private String message;

        public String getJobid() {
            return jobid;
        }

        public void setJobid(String jobid) {
            this.jobid = jobid;
        }

        public String getRealname() {
            return realname;
        }

        public void setRealname(String realname) {
            this.realname = realname;
        }

        public String getBankcard() {
            return bankcard;
        }

        public void setBankcard(String bankcard) {
            this.bankcard = bankcard;
        }

        public String getIdcard() {
            return idcard;
        }

        public void setIdcard(String idcard) {
            this.idcard = idcard;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getRes() {
            return res;
        }

        public void setRes(String res) {
            this.res = res;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
