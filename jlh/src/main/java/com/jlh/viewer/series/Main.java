package com.jlh.viewer.series;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * @author mymx.jlh
 * @version 1.0.0 2018/3/17 15:47
 */
public class Main {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File file = new File("obj.txt");
//        System.out.println(file.getAbsolutePath());
//        FileOutputStream fileOutputStream = new FileOutputStream(file);
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
//        Person person = new Person();
//        person.setName("jlh");
//        person.setOld("18");
//
//        objectOutputStream.writeObject(person);
//
//        objectOutputStream.close();
//        fileOutputStream.close();
        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        Person person = (Person) objectInputStream.readObject();

        System.out.println(person);
        objectInputStream.close();
        fileInputStream.close();

    }
}
