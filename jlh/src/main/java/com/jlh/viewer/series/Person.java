package com.jlh.viewer.series;

import java.io.Serializable;

/**
 * @author mymx.jlh
 * @version 1.0.0 2018/3/17 15:47
 */
public class Person implements Serializable{
    private static final long serialVersionUID = 2L;

    private String name;
    private String old;
    private String favor;

    public String getFavor() {
        return favor;
    }

    public void setFavor(String favor) {
        this.favor = favor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOld() {
        return old;
    }

    public void setOld(String old) {
        this.old = old;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", old='" + old + '\'' +
                ", favor='" + favor + '\'' +
                '}';
    }
}
