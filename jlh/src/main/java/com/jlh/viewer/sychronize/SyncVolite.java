package com.jlh.viewer.sychronize;

import java.util.concurrent.TimeUnit;

/**
 * @author mymx.jlh
 * @version 1.0.0 2018/5/3 11:49
 */
public class SyncVolite {

    private static boolean run = true;
    private static final Integer resource = 1;

    public static void main(String[] args) throws InterruptedException {

        new Thread(()->{
            while (true){
                synchronized (resource){
                    if (!run){
                        break;
                    }
                }
            }
            System.out.println("exit!");
        }).start();

        new Thread(()->{
            while (true){
                if (!run){
                    break;
                }
            }
            System.out.println("exit!");
        }).start();

        System.out.println("sleep");
        TimeUnit.SECONDS.sleep(3);
        System.out.println("awake");
        run = false;

    }
}
