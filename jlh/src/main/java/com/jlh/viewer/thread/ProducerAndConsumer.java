package com.jlh.viewer.thread;

import java.util.Random;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author mymx.jlh
 * @version 1.0.0 2018/4/21 9:42
 */
public class ProducerAndConsumer {

    public static LinkedBlockingQueue<String> msgs = new LinkedBlockingQueue<>();
    public static Random random = new Random(System.currentTimeMillis());

    public static void main(String[] args) {
        new Thread(()->{
            while (true){
                try {
                    TimeUnit.SECONDS.sleep(1);
                    String goods = String.format("goodsNO.%s", random.nextInt());
                    System.out.println("producer:" + goods);
                    msgs.put(goods);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();

        new Thread(()->{
            while (true){
                try {
                    String goods = msgs.take();
                    System.out.println("consumer:"+goods);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
