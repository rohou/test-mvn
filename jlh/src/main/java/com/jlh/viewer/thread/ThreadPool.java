package com.jlh.viewer.thread;

import java.util.concurrent.*;

/**
 * com.jlh.viewer.thread
 * Created by ASUS on 2017/4/25.
 * 13:36
 */
public class ThreadPool {
    public static void main(String[] args) {
        ExecutorService executorService = new ThreadPoolExecutor(5, 10,
                60L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(), (r, executor) -> {
                    executor.getQueue().poll();
                    executor.getQueue().offer(r);
                });
    }
}
