package com.jlh.viewer.thread;

import java.util.concurrent.TimeUnit;

/**
 * Created by jlh
 * On 2017/4/6 0006.
 */
public class ThreadWait {
    private static Thread th1,th2;
    private static int flag=0;
    public static void main(String[] args) throws InterruptedException {

        th1=new Thread(()->{
            for(;;){
                synchronized (th1) {
                    System.out.println("th1");
                    try {
                        if (flag==0)
                        th1.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        th1.start();
        TimeUnit.SECONDS.sleep(2);
        synchronized (th1){
            th1.notifyAll();
            TimeUnit.SECONDS.sleep(2);
            System.out.println(11111);
        }

    }
}
