package com.xx.base.phase1;

import com.xx.base.phase1.impl.LightDemo;

/**
 * author: XiongXin
 * description:
 * time: 2017/4/20.
 */
public class ChildClient extends LightDemo {

    public void execute(){
        System.out.println("子类开始执行");
        LightDemo demo = new LightDemo();
        protectedMethod();
        demo.publicMethod();
    }

}
