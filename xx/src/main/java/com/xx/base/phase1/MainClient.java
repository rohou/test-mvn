package com.xx.base.phase1;

import com.xx.base.phase1.client.DifferentPackageClient;
import com.xx.base.phase1.impl.LightDemo;
import com.xx.base.phase1.impl.SamePackageClient;

/**
 * author: XiongXin
 * description:
 * time: 2017/4/20.
 */
public class MainClient {
    public static void main(String[] args) {
        LightDemo demo = new LightDemo();
        demo.execute();
        SamePackageClient s = new SamePackageClient();
        s.execute();
        ChildClient c = new ChildClient();
        c.execute();
        DifferentPackageClient d = new DifferentPackageClient();
        d.execute();
    }
}
