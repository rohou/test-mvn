package com.xx.base.phase1.client;

import com.xx.base.phase1.impl.LightDemo;

/**
 * author: XiongXin
 * description:
 * time: 2017/4/20.
 */
public class DifferentPackageClient {

    public void execute(){
        System.out.println("不同包的类开始调用");
        LightDemo lightDemo = new LightDemo();
        lightDemo.publicMethod();
    }

}
