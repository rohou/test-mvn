package com.xx.base.phase1.impl;

/**
 * author: XiongXin
 * description:
 * time: 2017/4/20.
 */
public class LightDemo {

    private void privateMethod(){
        System.out.println("调用私有方法");
    }

    void defaultMethod(){
        System.out.println("调用默认方法");
    }

    public void publicMethod(){
        System.out.println("调用公有方法");
    }
    
    protected void protectedMethod(){
        System.out.println("调用受保护的方法");
    }

    public void execute(){
        LightDemo demo = new LightDemo();
        System.out.println("本类调用");
        demo.privateMethod();
        demo.defaultMethod();
        demo.protectedMethod();
        demo.publicMethod();
    }

}
