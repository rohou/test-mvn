package com.xx.base.phase1.impl;

/**
 * author: XiongXin
 * description:
 * time: 2017/4/20.
 */
public class SamePackageClient {
    public void execute(){
        System.out.println("同包客户端开始执行");
        LightDemo demo = new LightDemo();
        demo.defaultMethod();
        demo.protectedMethod();
        demo.publicMethod();
    }
}
